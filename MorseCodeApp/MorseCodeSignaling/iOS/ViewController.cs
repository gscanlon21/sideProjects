﻿using System;
// To use a dictionary
using System.Collections.Generic;
// To start a new task
using System.Threading.Tasks;
// For UI elements
using UIKit;
// To create shapes (sa. rectangle)
using CoreGraphics;
// Debug.WriteLine('using System.Diagnostics');
using System.Diagnostics;
// Play audio
using AudioToolbox;
// Play audio
using Foundation;
// Torch
using AVFoundation;


namespace MorseCodeSignaling.iOS
{
    public partial class ViewController : UIViewController
    {

        #region Class variables
        // Define UI elements      
        UISlider morseCodeSpeedSlider;
        UITextField morseCodeTextField;
        UISwitch morseCodeTypeSwitch;
        UILabel morseCodeOutput;
        UILabel morseCodeSpeedSliderLabel;

        // Define audio variables
        NSUrl url;
        SystemSound morseCodeBeep;

        NSMutableAttributedString morseCodeOutputString;

        // Playback speed
        private double morseCodeSpeed = 1d;

        // Cancel Morse Code execution
        private bool cancel;

        // Is Morse Code still executing?
        private bool running;

        // Morse Code dictionary
        Dictionary<string, string> morseCodeDict = new Dictionary<string, string>()
        {
            ["a"] = "01",
            ["b"] = "1000",
            ["c"] = "1010",
            ["d"] = "100",
            ["e"] = "0",
            ["f"] = "0010",
            ["g"] = "110",
            ["h"] = "0000",
            ["i"] = "00",
            ["j"] = "0111",
            ["k"] = "101",
            ["l"] = "0100",
            ["m"] = "11",
            ["n"] = "10",
            ["o"] = "111",
            ["p"] = "0110",
            ["q"] = "1101",
            ["r"] = "010",
            ["s"] = "000",
            ["t"] = "1",
            ["u"] = "001",
            ["v"] = "0001",
            ["w"] = "011",
            ["x"] = "1001",
            ["y"] = "1011",
            ["z"] = "1100",
            [" "] = "2",
            ["1"] = "01111",
            ["2"] = "00111",
            ["3"] = "00011",
            ["4"] = "00001",
            ["5"] = "00000",
            ["6"] = "10000",
            ["7"] = "11000",
            ["8"] = "11100",
            ["9"] = "11110",
            ["0"] = "11111"
        };
        #endregion

        #region Flash Morse Code
        private void FlashOn()
        /*
         * Flashlight flash on -- not tested
         */
        {
            /*
            Debug.WriteLine("FlashON!!!");
            var device = AVCaptureDevice.GetDefaultDevice(AVMediaType.Video);
            if (device == null)
            {
                return;
            }

            NSError error = null;
            device.LockForConfiguration(out error);
            if (error != null)
            {
                Debug.WriteLine(error);
                device.UnlockForConfiguration();
                return;
            }
            else
            {
                Debug.WriteLine("else initialed");
                if (device.TorchMode != AVCaptureTorchMode.On)
                {
                    Debug.WriteLine("Flashlight should be on");
                    device.TorchMode = AVCaptureTorchMode.On;
                }
                device.UnlockForConfiguration();
            }
            */
            // /*
            // AVCaptureDevice[] devices = AVCaptureDevice.DevicesWithMediaType(AVMediaType.Video); 
            // for (int i = 0; i < devices.Length; i++)
            Foundation.NSError error;
            for (int i = 0; i < AVCaptureDevice.Devices.Length; i++)
            {
                Debug.WriteLine("Has Device!!!");
                // AVCaptureDevice device = devices[i];                 
                AVCaptureDevice device = AVCaptureDevice.Devices[i];
                if (device.HasFlash == true)
                {
                    Debug.WriteLine("hasFlash!!!");
                    device.LockForConfiguration(out error);
                    device.TorchMode = AVCaptureTorchMode.On;
                    device.UnlockForConfiguration();
                }
            }
            //*/
        }

        private void FlashOff()
        /*
         * Flashlight flash off -- not tested
         */
        {
            // AVCaptureDevice[] devices = AVCaptureDevice.DevicesWithMediaType(AVMediaType.Video); 
            // for (int i = 0; i < devices.Length; i++)
            Foundation.NSError error;
            for (int i = 0; i < AVCaptureDevice.Devices.Length; i++)
            {
                // AVCaptureDevice device = devices[i];                 
                AVCaptureDevice device = AVCaptureDevice.Devices[i];
                if (device.HasFlash == true)
                {
                    device.LockForConfiguration(out error);
                    device.TorchMode = AVCaptureTorchMode.Off;
                    device.UnlockForConfiguration();
                }
            }
        }

        async private void FlashMorseCode(string text)
        /*
         * Uses the flashlight for morse code 
         */
        {
            // This function is now running
            running = true;

            // Converts text to all lowercase
            text = text.ToLower();

            foreach (char c in text)
            {
                string morseCode;
                if (!morseCodeDict.TryGetValue(c.ToString(), out morseCode))
                {
                    // The key isn't in the dictionary.
                    morseCode = "2";
                }

                foreach (char chr in morseCode)
                /*
                 * Iterate over the morse code for a particular character
                 */
                {
                    if (chr == '0')
                    {
                        // Flashlight flash
                        FlashOn();
                        var timeToSleep = (int)(100 / morseCodeSpeed);
                        await Task.Delay(timeToSleep); // Wait default .1 seconds without blocking
                        FlashOff();
                    }
                    else if (chr == '1')
                    {
                        // Flashlight flash
                        FlashOn();
                        var timeToSleep = (int)(300 / morseCodeSpeed);
                        await Task.Delay(timeToSleep); // Wait default .3 seconds without blocking
                        FlashOff();
                    }
                    else
                    /* 
                     * The letter space: " "
                     */
                    {
                        // Time to sleep between words, combines with time between letters to make default .7 sec delay
                        var timeToSleep = (int)(400 / morseCodeSpeed);
                        await Task.Delay(timeToSleep); // Wait default .4 seconds without blocking
                    }
                }

                // Time to sleep between letters
                var timeToSleepBetweenLeters = (int)(300 / morseCodeSpeed);
                await Task.Delay(timeToSleepBetweenLeters); // Wait default .3 seconds without blocking
            }

            // This function has completed
            running = false;
        }
        #endregion

        #region OnReturn(string text)
        private void OnReturn(string text)
        {
            DisplayMorseCode(text);

            if (morseCodeTypeSwitch.On)
            /*
             * Uses torch for Morse Code
             */
            {
                FlashMorseCode(text);
            }
            else
            /*
             * Uses audio for Morse Code
             */
            {
                SoundMorseCode(text);
            }
        }
        #endregion

        #region SoundMorseCode
        async private void SoundMorseCode(string text)
        /* 
         * Uses audio file to output morse code
         * Combine this and FlashMorseCode into one function to remove duplicate code
         */
        {
            // This function is now running
            running = true;

            // Delay to accomadate startup lag (HAPPENS ON SIMULATOR, COMMENT OUT AND TEST FOR REAL DEVICE)
            await Task.Delay(100); // Wait .1 seconds

            // Colour the Morse Code character that is currently playing
            int range = 0;

            var currentCharacter = new UIStringAttributes
            {
                ForegroundColor = UIColor.Red
            };

            var prettystring = new NSMutableAttributedString("");

            // Convert text to lowercase
            text = text.ToLower();

            // Reset cancel button if it was previously pressed
            if (cancel)
            {
                cancel = false;
            }

            foreach (char c in text)
            /*
             * Iterate over characters in string text
             */
            {
                string morseCode;
                if (!morseCodeDict.TryGetValue(c.ToString(), out morseCode))
                {
                    // The key isn't in the dictionary.
                    morseCode = "2";
                }

                foreach (char chr in morseCode)
                /*
                 * Iterate over the morse code for a particular character
                 */
                {
                    if (!cancel)
                    {
                        if (chr == '0')
                        {
                            // Play sound effect
                            morseCodeBeep.PlaySystemSound();
                            prettystring.SetString(morseCodeOutputString);
                            prettystring.SetAttributes(currentCharacter.Dictionary, new NSRange(range, 1));
                            morseCodeOutput.AttributedText = @prettystring;
                            var timeToSleep = (int)(100 / morseCodeSpeed);
                            await Task.Delay(timeToSleep); // Wait default .1 seconds without blocking
                        }
                        else if (chr == '1')
                        {
                            // Play sound effect
                            prettystring.SetString(morseCodeOutputString);
                            prettystring.SetAttributes(currentCharacter.Dictionary, new NSRange(range, 1));
                            morseCodeOutput.AttributedText = @prettystring;
                            morseCodeBeep.PlaySystemSound();
                            var timeToSleep = (int)(300 / morseCodeSpeed);
                            await Task.Delay(timeToSleep); // Wait default .3 seconds without blocking
                        }
                        else
                        /*
                         * The letter space: " "
                         */
                        {
                            // Accounts for double spaced spaces
                            range++;
                            // Time to sleep between words, combines with time between letters to make default .7 sec delay
                            var timeToSleep = (int)(400 / morseCodeSpeed);
                            await Task.Delay(timeToSleep); // Wait default .4 seconds without blocking
                        }
                        range++;
                    }
                    else
                    {
                        return;
                    }
                }

                // Time to sleep between letters
                var timeToSleepBetweenLeters = (int)(300 / morseCodeSpeed);
                await Task.Delay(timeToSleepBetweenLeters); // Wait default .3 seconds without blocking
            }

            // This function has completed
            running = false;
        }
        #endregion

        #region DisplayMorseCode
        private void DisplayMorseCode(string text)
        /* 
         * Displays Morse Code translation on the screen
         */
        {
            // Convert text to lowercase
            text = text.ToLower();

            // Output String
            morseCodeOutputString = new NSMutableAttributedString("");

            foreach (char c in text)
            /*
             * Iterate over characters in string text
             */
            {
                string morseCode;
                if (!morseCodeDict.TryGetValue(c.ToString(), out morseCode))
                {
                    // The key isn't in the dictionary.
                    morseCode = "2";
                }

                foreach (char chr in morseCode)
                /*
                 * Iterate over the morse code for a particular character
                 */
                {
                    if (chr == '0')
                    {
                        morseCodeOutputString.Append(new NSMutableAttributedString("•"));
                    }
                    else if (chr == '1')
                    {
                        morseCodeOutputString.Append(new NSMutableAttributedString("-"));
                    }
                    else
                    /*
                     * The letter space: " "
                     */
                    {
                        // Two spaces for visual clarity
                        morseCodeOutputString.Append(new NSMutableAttributedString("  "));
                    }
                }
            }

            // Sets the output text to the Morse Code translation
            morseCodeOutput.AttributedText = @morseCodeOutputString;
        }
        #endregion

        public override void ViewDidLoad()
        /*
         * This function returns nothing
         */
        {
            #region Base
            base.ViewDidLoad();
            Title = "Morse Code";
            View.BackgroundColor = UIColor.LightGray;

            var width = UIScreen.MainScreen.Bounds.Width;
            var height = UIScreen.MainScreen.Bounds.Height;

            // Code to start the Xamarin Test Cloud Agent
#if ENABLE_TEST_CLOUD
            Xamarin.Calabash.Start();
#endif
            #endregion

            #region Perform any additional setup after loading the view, typically from a nib

            #region morseCodeBeep
            // Get audio location
            url = NSUrl.FromFilename("Sounds/e.wav");
            // Load audio to variable
            morseCodeBeep = new SystemSound(url);
            #endregion

            #region morseCodeTextField
            morseCodeTextField = new UITextField(new CGRect(10, 20, width - 20, 60)); // x, y, width, height
            morseCodeTextField.BackgroundColor = UIColor.LightGray;
            morseCodeTextField.ClearsOnInsertion = true;
            morseCodeTextField.ClearButtonMode = UITextFieldViewMode.Always;
            morseCodeTextField.Placeholder = "Enter string to be Morse Coded";
            morseCodeTextField.ShouldReturn += HandleMorseCodeTextField;
            View.Add(morseCodeTextField);
            #endregion

            #region returnButton
            var returnButton = UIButton.FromType(UIButtonType.System);
            returnButton.SetTitle("Click to signal in Morse Code", UIControlState.Normal);
            returnButton.Frame = new CGRect(10, 100, (width / 2) + 30, 40); // x, y, width, height
            returnButton.AccessibilityIdentifier = "ConvertToMorseCode";
            returnButton.TouchUpInside += HandleReturnButtonTouchUpInside;
            View.AddSubview(returnButton);
            #endregion

            #region cancelButton
            var cancelButton = UIButton.FromType(UIButtonType.System);
            cancelButton.SetTitle("Cancel", UIControlState.Normal);
            cancelButton.Frame = new CGRect(width - 80, 100, 60, 40); // x, y, width, height
            cancelButton.AccessibilityIdentifier = "CancelMorseCodeExecution";
            cancelButton.TouchUpInside += HandleCancelButtonTouchUpInside;
            View.AddSubview(cancelButton);
            #endregion

            #region morseCodeSpeedSlider
            morseCodeSpeedSlider = new UISlider(new CGRect(10, 200, width - 20, 20)); // x, y, width, height
            morseCodeSpeedSlider.MinValue = .1f;
            morseCodeSpeedSlider.MaxValue = 1.5f;
            morseCodeSpeedSlider.Value = 1f;
            morseCodeSpeedSlider.ValueChanged += HandleMorseCodeSpeedSliderValueChanged;
            View.Add(morseCodeSpeedSlider);
            #endregion

            #region morseCodeSpeedSliderLabel
            morseCodeSpeedSliderLabel = new UILabel(new CGRect(10, 260, width - 20, 20)); // x, y, width, height
            morseCodeSpeedSliderLabel.Text = $"Morse Code speed: {morseCodeSpeedSlider.Value}";
            View.Add(morseCodeSpeedSliderLabel);
            #endregion

            #region morseCodeTypeSwitch
            morseCodeTypeSwitch = new UISwitch(new CGRect(10, 400, 100, 20)); // x, y, width, height
            morseCodeTypeSwitch.SetState(false, true);
            View.Add(morseCodeTypeSwitch);
            #endregion

            #region morseCodeTypeSwitchLabel
            var morseCodeTypeSwitchLabel = new UILabel(new CGRect(10, 480, 300, 20)); // x, y, width, height
            morseCodeTypeSwitchLabel.Text = "Use flashlight instead of audio";
            View.Add(morseCodeTypeSwitchLabel);
            #endregion

            #region morseCodeOutput
            var boundry = new CGRect(10, height - 110, width - 20, 100);
            morseCodeOutput = new UILabel(boundry); // x, y, width, height
            morseCodeOutput.Lines = 4;
            morseCodeOutput.TextAlignment = UITextAlignment.Center;
            morseCodeOutput.LineBreakMode = UILineBreakMode.WordWrap;
            morseCodeOutput.Text = "";
            View.Add(morseCodeOutput);
            #endregion

            #endregion

            #region Layout Options
            // Focus on the textfield
            morseCodeTextField.BecomeFirstResponder();
            morseCodeTextField.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            returnButton.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            morseCodeSpeedSlider.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            morseCodeSpeedSliderLabel.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            cancelButton.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            morseCodeTypeSwitch.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            morseCodeTypeSwitchLabel.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            #endregion
        }

        #region HandleMorseCodeTextField[Input]
        private bool HandleMorseCodeTextFieldInput(object sender)
        /* 
         * Handles the morseCodeTextField input validation
         */
        {
            if (((UITextField)sender).Text.Length <= 0)
            /*
             * Perform a simple "required" validation
             */
            {
                InvokeOnMainThread(() =>
                /*
                 * Need to update on the main thread to change the border color
                 */
                {
                    // morseCodeTextField.BackgroundColor = UIColor.Yellow;
                    morseCodeTextField.Layer.BorderColor = UIColor.Red.CGColor;
                    morseCodeTextField.Layer.BorderWidth = 3;
                    morseCodeTextField.Layer.CornerRadius = 5;
                });
                return false;
            }
            else
            /*
             * Restore "ok" textfield settings
             */
            {
                InvokeOnMainThread(() =>
                /*
                 * Need to update on the main thread to change the border color
                 */
                {
                    // morseCodeTextField.BackgroundColor = UIColor.LightGray;
                    morseCodeTextField.Layer.BorderColor = UIColor.LightGray.CGColor;
                    morseCodeTextField.Layer.BorderWidth = 0;
                    // morseCodeTextField.Layer.CornerRadius = 5;
                });
                return true;
            }
        }

        private bool HandleMorseCodeTextField(object sender)
        /* 
         * Handles the keyboard RETURN button pressed for the morseCodeTextField
         */
        {

            if (HandleMorseCodeTextFieldInput(sender) && !running)
            {
                var text = morseCodeTextField.Text;
                OnReturn(text);
            }
            else
            {
                // Add no text inputted error handling
                return false;
            }
            // Unfocuses from the textfield and dismisses the keyboard
            morseCodeTextField.ResignFirstResponder();
            return true;
        }
        #endregion

        #region HandleMorseCodeSlider
        private void HandleMorseCodeSpeedSliderValueChanged(object sender, EventArgs e)
        {
            morseCodeSpeed = Math.Round(morseCodeSpeedSlider.Value, 1, MidpointRounding.AwayFromZero);
            morseCodeSpeedSlider.Value = (float)morseCodeSpeed;
            morseCodeSpeedSliderLabel.Text = $"Morse Code speed: {morseCodeSpeed}";
        }
        #endregion

        #region HandleCancelButton
        private void HandleCancelButtonTouchUpInside(object sender, EventArgs e)
        /*
         * Handles the UI cancelButton when pressed
         */
        {
            cancel = true;
            running = false;
        }
        #endregion

        #region HandleReturnButton
        private void HandleReturnButtonTouchUpInside(object sender, EventArgs e)
        /*
         * Handles the UI returnButton when pressed
         */
        {
            if (HandleMorseCodeTextFieldInput(morseCodeTextField) && !running)
            {
                var text = morseCodeTextField.Text;
                OnReturn(text);
            }
            else
            {
                return;
            }
            // Unfocuses from the textfield and dismisses the keyboard
            morseCodeTextField.ResignFirstResponder();
        }
        #endregion

        #region system
        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.     
        }

        public ViewController(IntPtr handle) : base(handle)
        {
        }
        #endregion
    }
}
