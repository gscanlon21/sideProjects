﻿using Android.App;
// UI elements
using Android.Widget;
using Android.OS;
using Android.Views;
// Play audio
using Android.Media;
// To use a dictionary
using System.Collections.Generic;
// To start a new task
using System.Threading.Tasks;


namespace MorseCodeSignaling.Droid
{
    [Activity(Label = "MorseCodeSignaling", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {

        #region Class variables
        // Make buttons function specific
        // Define UI widgets
        Button cancelButton;
        Button returnButton;
        Switch morseCodeTypeSwitch;
        EditText morseCodeTextField;
        SeekBar morseCodeSpeedSlider;
        TextView morseCodeOutput;
        TextView morseCodeSpeedSliderLabel;

        // Play audio
        MediaPlayer morseCodeBeep;

        // Playback speed
        private float morseCodeSpeed;

        // Cancel Morse Code execution
        private bool cancel;

        // Is Morse Code still executing
        private bool running;

        // Morse Code dictionary
        Dictionary<string, string> morseCodeDict = new Dictionary<string, string>()
        {
            ["a"] = "01",
            ["b"] = "1000",
            ["c"] = "1010",
            ["d"] = "100",
            ["e"] = "0",
            ["f"] = "0010",
            ["g"] = "110",
            ["h"] = "0000",
            ["i"] = "00",
            ["j"] = "0111",
            ["k"] = "101",
            ["l"] = "0100",
            ["m"] = "11",
            ["n"] = "10",
            ["o"] = "111",
            ["p"] = "0110",
            ["q"] = "1101",
            ["r"] = "010",
            ["s"] = "000",
            ["t"] = "1",
            ["u"] = "001",
            ["v"] = "0001",
            ["w"] = "011",
            ["x"] = "1001",
            ["y"] = "1011",
            ["z"] = "1100",
            [" "] = "2",
            ["1"] = "01111",
            ["2"] = "00111",
            ["3"] = "00011",
            ["4"] = "00001",
            ["5"] = "00000",
            ["6"] = "10000",
            ["7"] = "11000",
            ["8"] = "11100",
            ["9"] = "11110",
            ["0"] = "11111"
        };
        #endregion

        #region OnReturn(string text)
        private void OnReturn(string text)
        {
            DisplayMorseCode(text);

            if (morseCodeTypeSwitch.Checked)
            /*
             * Uses torch for Morse Code
             */
            {
                FlashMorseCode(text);
            }
            else
            /*
             * Uses audio for Morse Code
             */
            {
                SoundMorseCode(text);
            }
        }
        #endregion

        #region Flash Morse Code
        private void FlashOn()
        /*
         * Flashlight flash on -- not tested
         */
        {
        }

        private void FlashOff()
        /*
         * Flashlight flash off -- not tested
         */
        {
        }

        async private void FlashMorseCode(string text)
        /*
         * Uses the flashlight for morse code 
         */
        {
            // This function is now running
            running = true;

            // Converts text to all lowercase
            text = text.ToLower();

            foreach (char c in text)
            {
                string morseCode;
                if (!morseCodeDict.TryGetValue(c.ToString(), out morseCode))
                {
                    // The key isn't in the dictionary.
                    morseCode = "2";
                }

                foreach (char chr in morseCode)
                /*
                 * Iterate over the morse code for a particular character
                 */
                {
                    if (chr == '0')
                    {
                        // Flashlight flash
                        FlashOn();
                        var timeToSleep = (int)(100 / morseCodeSpeed);
                        await Task.Delay(timeToSleep); // Wait default .1 seconds without blocking
                        FlashOff();
                    }
                    else if (chr == '1')
                    {
                        // Flashlight flash
                        FlashOn();
                        var timeToSleep = (int)(300 / morseCodeSpeed);
                        await Task.Delay(timeToSleep); // Wait default .3 seconds without blocking
                        FlashOff();
                    }
                    else
                    /* 
                     * The letter space: " "
                     */
                    {
                        // Time to sleep between words, combines with time between letters to make default .7 sec delay
                        var timeToSleep = (int)(400 / morseCodeSpeed);
                        await Task.Delay(timeToSleep); // Wait default .4 seconds without blocking
                    }
                }

                // Time to sleep between letters
                var timeToSleepBetweenLeters = (int)(300 / morseCodeSpeed);
                await Task.Delay(timeToSleepBetweenLeters); // Wait default .3 seconds without blocking
            }

            // This function has completed
            running = false;
        }
        #endregion

        #region DisplayMorseCode
        private void DisplayMorseCode(string text)
        {
            // Convert text to lowercase
            text = text.ToLower();

            // Output String
            var morseCodeOutputString = "";

            foreach (char c in text)
            {
                string morseCode;
                if (!morseCodeDict.TryGetValue(c.ToString(), out morseCode))
                {
                    // The key isn't in the dictionary.
                    morseCode = "2";
                }

                foreach (char chr in morseCode)
                {
                    if (chr == '0')
                    {
                        // Morse Code dot
                        morseCodeOutputString += "•";
                    }
                    else if (chr == '1')
                    {
                        // Morse Code dash
                        morseCodeOutputString += "-";
                    }
                    else
                    {
                        // Two spaces for visual clarity
                        morseCodeOutputString += "  ";
                    }
                }
            }

            // Sets the output text to the Morse Code translation
            morseCodeOutput.Text = @morseCodeOutputString;
        }
        #endregion

        #region SoundMorseCode
        async private void SoundMorseCode(string text)
        /* 
         * Uses audio file to output morse code
         * Combine this and FlashMorseCode into one function to remove duplicate code
         * FIXME - App force closes after one beep - only on Android
         * TODO - added coloured code to display what character of morse code is playing
         */
        {
            // This function is now running
            running = true;

            // Colour the Morse Code character that is currently playing
            int range = 0;

            // Delay to accomadate startup lag (HAPPENS ON SIMULATOR, COMMENT OUT AND TEST FOR REAL DEVICE)
            await Task.Delay(100); // Wait .1 seconds

            // Convert text to lowercase
            text = text.ToLower();

            //var currentCharacter = new UIStringAttributes
            //{
            //    ForegroundColor = UIColor.Red
            //};

            //var prettystring = new NSMutableAttributedString("");

            // Reset cancel button if it was previously pressed
            if (cancel)
            {
                cancel = false;
            }

            foreach (char c in text)
            /*
             * Iterate over characters in string text
             */
            {
                string morseCode;
                if (!morseCodeDict.TryGetValue(c.ToString(), out morseCode))
                {
                    // The key isn't in the dictionary.
                    morseCode = "2";
                }

                foreach (char chr in morseCode)
                /*
                 * Iterate over the morse code for a particular character
                 */
                {
                    if (!cancel)
                    {
                        if (chr == '0')
                        {
                            // Play sound effect
                            /*
                            prettystring.SetString(morseCodeOutputString);
                            prettystring.SetAttributes(currentCharacter.Dictionary, new NSRange(range, 1));
                            morseCodeOutput.AttributedText = @prettystring;
                            */
                            morseCodeBeep.Start();
                            var timeToSleep = (int)(100 / morseCodeSpeed);
                            await Task.Delay(timeToSleep); // Wait default .1 seconds without blocking
                        }
                        else if (chr == '1')
                        {
                            // Play sound effect
                            /*
                            prettystring.SetString(morseCodeOutputString);
                            prettystring.SetAttributes(currentCharacter.Dictionary, new NSRange(range, 1));
                            morseCodeOutput.AttributedText = @prettystring;
                            */
                            morseCodeBeep.Start();
                            var timeToSleep = (int)(300 / morseCodeSpeed);
                            await Task.Delay(timeToSleep); // Wait default .3 seconds without blocking
                        }
                        else
                        /*
                         * The letter space: " "
                         */
                        {
                            // range++;
                            // Time to sleep between words, combines with time between letters to make default .7 sec delay
                            var timeToSleep = (int)(400 / morseCodeSpeed);
                            await Task.Delay(timeToSleep); // Wait default .4 seconds without blocking
                        }
                        range++;
                    }
                    else
                    {
                        return;
                    }
                }

                // Time to sleep between letters
                var timeToSleepBetweenLeters = (int)(300 / morseCodeSpeed);
                await Task.Delay(timeToSleepBetweenLeters); // Wait default .3 seconds without blocking
            }

            // This function has completed
            running = false;
        }
        #endregion

        protected override void OnCreate(Bundle savedInstanceState)
        {

            #region Xamarin.Forms cross-platform
            /*
            base.OnCreate(savedInstanceState);

            Forms.Init(this, savedInstanceState);

            LoadApplication (new App ());
            */
            #endregion

            #region Base
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get the width and the height
            var metrics = Resources.DisplayMetrics;
            var width = metrics.WidthPixels;
            var height = metrics.HeightPixels;
            #endregion

            #region morseCodeOutput
            morseCodeOutput = FindViewById<TextView>(Resource.Id.morseCodeOutput);
            morseCodeOutput.SetLines(4); // Also TextView.SetMaxLines(int)
            morseCodeOutput.Text = "";
            //morseCodeOutput.TextAlignment = UITextAlignment.Center;
            //morseCodeOutput.LineBreakMode = UILineBreakMode.WordWrap;
            #endregion

            #region morseCodeBeep
            // Load Morse Code audio
            morseCodeBeep = MediaPlayer.Create(this, Resource.Raw.e);
            #endregion

            #region cancelButton
            cancelButton = FindViewById<Button>(Resource.Id.cancelButton);
            cancelButton.Click += HandleCancelButtonClick;
            cancelButton.Text = "Cancel";
            #endregion

            #region morseCodeTextField
            morseCodeTextField = FindViewById<EditText>(Resource.Id.morseCodeTextField);
            morseCodeTextField.KeyPress += HandleMorseCodeTextField;
            morseCodeTextField.Hint = "Enter string to be Morse Coded";
            //morseCodeTextField.ClearsOnInsertion = true;
            #endregion

            #region morseCodeTypeSwitch
            morseCodeTypeSwitch = FindViewById<Switch>(Resource.Id.morseCodeTypeSwitch);
            morseCodeTypeSwitch.Checked = false;
            morseCodeTypeSwitch.Text = "Use flashlight instead of audio to signal morse code";
            #endregion

            #region returnButton
            returnButton = FindViewById<Button>(Resource.Id.returnButton);
            returnButton.Click += HandleReturnButton;
            returnButton.Text = "Click to be morse coded";
            #endregion

            #region morseCodeSpeedSlider
            morseCodeSpeedSlider = FindViewById<SeekBar>(Resource.Id.morseCodeSpeedSlider);
            morseCodeSpeedSlider.ProgressChanged += HandleMorseCodeSpeedSliderValueChanged;
            morseCodeSpeedSlider.Max = 15;
            morseCodeSpeedSlider.Progress = 10;
            //morseCodeSpeedSlider.MinValue = .1f; // Android's cannot be changed from 0
            #endregion

            #region morseCodeSpeedSliderLabel
            morseCodeSpeedSliderLabel.Text = $"Morse Code speed: {morseCodeSpeedSlider.Progress}";
            #endregion
        }

        #region HandleMorseCodeTextField[Input]
        private bool HandleMorseCodeTextFieldInput(object sender)

        {
            if (((TextView)sender).Text.Length <= 0)

            {
                //InvokeOnMainThread(() =>

                //{
                //morseCodeTextField.Layer.BorderColor = UIColor.Red.CGColor;
                //morseCodeTextField.Layer.BorderWidth = 3;
                //morseCodeTextField.Layer.CornerRadius = 5;
                //});
                return false;
            }
            else

            {
                //InvokeOnMainThread(() =>

                //{
                // morseCodeTextField.BackgroundColor = UIColor.White;
                //morseCodeTextField.Layer.BorderColor = UIColor.LightGray.CGColor;
                //morseCodeTextField.Layer.BorderWidth = 0;
                // morseCodeTextField.Layer.CornerRadius = 5;
                //});
                return true;
            }
        }

        private bool HandleMorseCodeTextField(object sender)

        {

            if (HandleMorseCodeTextFieldInput(sender) && !running)
            {
                var text = morseCodeTextField.Text;
                OnReturn(text);
            }
            else
            {
                // Add no text inputted error handling
                return false;
            }
            // Unfocuses from the textfield and dismisses the keyboard
            //morseCodeTextField.ResignFirstResponder();
            return true;
        }
        #endregion

        #region HandleMorseCodeSpeedSliderValueChanged
        private void HandleMorseCodeSpeedSliderValueChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            if (e.FromUser)
            {
                morseCodeSpeed = e.Progress;
                // morseCodeSpeed = Math.Round(morseCodeSpeedSlider.Value, 1, MidpointRounding.AwayFromZero);
                // morseCodeSpeedSlider.Value = (float)morseCodeSpeed;
                // morseCodeSpeedSliderLabel.Text = $"Morse Code speed: {morseCodeSpeed}";
            }
        }
        #endregion

        #region HandleReturnButton
        private void HandleReturnButton(object sender, object e)
        /*
         * Handles the UI returnButton when pressed
         */
        {
            if (HandleMorseCodeTextFieldInput(morseCodeTextField) && !running)
            {
                var text = morseCodeTextField.Text;
                OnReturn(text);
            }
            else
            {
                return;
            }
            // Unfocuses from the textfield and dismisses the keyboard
            //morseCodeTextField.ResignFirstResponder();
        }
        #endregion

        #region HandleCancelButton
        private void HandleCancelButtonClick(object sender, object e)
        /*
         * Handles the UI cancelButton when pressed
         */
        {
            cancel = true;
            running = false;
        }
        #endregion

        #region HandleMorseCodeTextField
        private void HandleMorseCodeTextField(object sender, View.KeyEventArgs e)
        /* 
         * Handles the keyboard RETURN button pressed for the morseCodeTextField
         */
        {
            e.Handled = false;
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                if (morseCodeTextField.Length() > 0 && !running)
                {
                    var text = morseCodeTextField.Text;
                    OnReturn(text);
                }
                else
                {
                    // Add no text inputted error handling
                }
                // Unfocuses from the textfield and dismisses the keyboard
                // morseCodeTextField.ResignFirstResponder();

                e.Handled = true;
            }
            // return true;
        }
        #endregion
    }
}
