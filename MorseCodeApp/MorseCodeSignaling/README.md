# About #

Morse Code Signaler written with C# using the native Xamarin.iOS and Xamarin.Android APIs

* Features:
    - Hear Morse Code
    - See Morse Code using the device's torch
    - See Morse Code on the device's screen
    - Adjust the speed of the Morse Code output

### TODO ###

* Unit Tests
* Android
    - App force closes after 1 beep

<br>
* General
    - Test torch flashing

<br>

### Data Sources ###

* Built using [C#](https://docs.microsoft.com/en-us/dotnet/csharp/csharp)
* Uses [Xamarin](https://developer.xamarin.com/) to hook into the phone's native functionality

### Who do I talk to? ###

Graham Scanlon - gscanlon21@gmail.com

### LICENSE ###

MIT

### Release Notes ###

* Dev Build 2
    - The app now displays the Morse Code on screen
    - The on-screen Morse Code has coloured text to tell the user where they are in the output

<br>
* Dev Build 1
    - iOS prototype built
