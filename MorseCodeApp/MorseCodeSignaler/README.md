# About #

Morse Code Signaler written with C# using the cross-platform Xamarin.Forms API

* Features:
    - Hear Morse Code
    - See Morse Code using the device's torch
    - See Morse Code on the device's screen
    - Adjust the speed of the Morse Code output

### TODO ###

* Write for cross-platform using Xamarin.Forms
* Unit Tests
* Implement platform-specific audio for morse code beep since Xamarin.Forms doesn't have a cross-platform API for audio
* Display Morse Code using the phone's torch

<br>

### Data Sources ###

* Built using [C#](https://docs.microsoft.com/en-us/dotnet/csharp/csharp)
* Uses [Xamarin](https://developer.xamarin.com/) to hook into the phone's native functionality

### Who do I talk to? ###

Graham Scanlon - gscanlon21@gmail.com

### LICENSE ###

MIT

### Release Notes ###

* Dev Build 1 -- in progress
       - Prototype is mostly completed
