﻿//
// MorseCodeSignalerPage.xaml.cs
//
// Author:
//       Graham Scanlon <gscanlon21@gmail.com>
//
// Copyright (c) 2017 Graham Scanlon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
using Xamarin.Forms;
using System;
// To use a dictionary
using System.Collections.Generic;
// To start a new task
using System.Threading.Tasks;
// Debug.WriteLine('using System.Diagnostics');
using System.Diagnostics;


namespace MorseCodeSignaler
{
    public partial class MorseCodeSignalerPage : ContentPage
    {

        #region Class variables
        // Define UI elements      
        Slider morseCodeSpeedSlider;
        Entry morseCodeTextField;
        Switch morseCodeTypeSwitch;
        Label morseCodeOutput;
        Label morseCodeSpeedSliderLabel;

        // Define audio variables
        //NSUrl url;
        //SystemSound morseCodeBeep;

        string normalTextBegin;
        string highlightedText;
        string normalTextEnd;
        FormattedString morseCodeOutputString
        {
            get
            {
                return new FormattedString
                {
                    Spans =
                    {
                        new Span
                        {
                            Text = @normalTextBegin,
                            ForegroundColor = Color.Black
                        },
                        new Span
                        {
                            Text = @highlightedText,
                            ForegroundColor = Color.Red
                        },
                        new Span
                        {
                            Text = @normalTextEnd,
                            ForegroundColor = Color.Black
                        }
                    }
                };
            }
        }

        // Playback speed
        private double morseCodeSpeed = 1d;

        // Cancel Morse Code execution
        private bool cancel;

        // Is Morse Code still executing?
        private bool running;

        // Morse Code dictionary
        Dictionary<string, string> morseCodeDict = new Dictionary<string, string>()
        {
            ["a"] = "01",
            ["b"] = "1000",
            ["c"] = "1010",
            ["d"] = "100",
            ["e"] = "0",
            ["f"] = "0010",
            ["g"] = "110",
            ["h"] = "0000",
            ["i"] = "00",
            ["j"] = "0111",
            ["k"] = "101",
            ["l"] = "0100",
            ["m"] = "11",
            ["n"] = "10",
            ["o"] = "111",
            ["p"] = "0110",
            ["q"] = "1101",
            ["r"] = "010",
            ["s"] = "000",
            ["t"] = "1",
            ["u"] = "001",
            ["v"] = "0001",
            ["w"] = "011",
            ["x"] = "1001",
            ["y"] = "1011",
            ["z"] = "1100",
            [" "] = "2",
            ["1"] = "01111",
            ["2"] = "00111",
            ["3"] = "00011",
            ["4"] = "00001",
            ["5"] = "00000",
            ["6"] = "10000",
            ["7"] = "11000",
            ["8"] = "11100",
            ["9"] = "11110",
            ["0"] = "11111"
        };
        #endregion

        public MorseCodeSignalerPage()
        {
            // InitializeComponent();

            #region Base
            Title = "Morse Code";

            //var width = Application.Current.MainPage.Width;
            //var height = Application.Current.MainPage.Height;

            // Code to start the Xamarin Test Cloud Agent
#if ENABLE_TEST_CLOUD
                                    Xamarin.Calabash.Start();
#endif
            #endregion

            #region Perform any additional setup after loading the view, typically from a nib

            #region morseCodeBeep
            // Get audio location
            //url = NSUrl.FromFilename("Sounds/e.wav");
            // Load audio to variable
            //morseCodeBeep = new SystemSound(url);
            #endregion

            #region morseCodeTextField
            morseCodeTextField = new Entry
            {
                Placeholder = "Enter string to be Morse Coded",
                IsPassword = false
            };
            // morseCodeTextField = new UITextField(new CGRect(10, 20, width - 20, 60)); // x, y, width, height
            morseCodeTextField.Completed += HandleMorseCodeTextField;
            #endregion

            #region returnButton
            var returnButton = new Button
            {
                Text = "Click to signal in Morse Code",
                Font = Font.SystemFontOfSize(NamedSize.Large),
                BorderWidth = 1,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };
            returnButton.Clicked += HandleReturnButtonPress;
            // returnButton.Frame = new CGRect(10, 100, (width / 2) + 30, 40); // x, y, width, height
            #endregion

            #region cancelButton
            var cancelButton = new Button
            {
                Text = "Cancel",
                Font = Font.SystemFontOfSize(NamedSize.Large),
                BorderWidth = 1,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };
            cancelButton.Clicked += HandleCancelButtonPress;
            // cancelButton.Frame = new CGRect(width - 80, 100, 60, 40); // x, y, width, height
            #endregion

            #region morseCodeSpeedSlider
            morseCodeSpeedSlider = new Slider(.1f, 1.5f, 1f);
            // new CGRect(10, 200, width - 20, 20) // x, y, width, height
            morseCodeSpeedSlider.ValueChanged += HandleMorseCodeSpeedSliderValueChanged;
            #endregion

            #region morseCodeSpeedSliderLabel
            morseCodeSpeedSliderLabel = new Label
            {
                Text = $"Morse Code speed: {morseCodeSpeedSlider.Value}",
                FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
            };
            // morseCodeSpeedSliderLabel = new Label(new CGRect(10, 260, width - 20, 20)); // x, y, width, height
            #endregion

            #region morseCodeTypeSwitch
            morseCodeTypeSwitch = new Switch
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };
            // morseCodeTypeSwitch = new Switch(new CGRect(10, 400, 100, 20)); // x, y, width, height // .Toggled
            #endregion

            #region morseCodeTypeSwitchLabel
            var morseCodeTypeSwitchLabel = new Label
            {
                Text = "Use flashlight instead of audio",
                FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
            };
            // var morseCodeTypeSwitchLabel = new Label(new CGRect(10, 480, 300, 20)); // x, y, width, height
            #endregion

            #region morseCodeOutput
            morseCodeOutput = new Label
            {
                FormattedText = "",
                FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                LineBreakMode = LineBreakMode.WordWrap,
                HorizontalTextAlignment = TextAlignment.Center,
            };
            #endregion

            #endregion

            #region Display Content
            // Accomodate iPhone status bar.
            int yPadding;
            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    yPadding = 20;
                    break;
                default:
                    yPadding = 0;
                    break;
            }
            this.Padding = new Thickness(10, yPadding, 10, 5);

            Content = new StackLayout
            {
                Spacing = 2,
                Padding = 2,
                Margin = 2,
                Children =
                {
                    morseCodeTextField,
                    returnButton,
                    cancelButton,
                    morseCodeSpeedSlider,
                    morseCodeSpeedSliderLabel,
                    morseCodeTypeSwitch,
                    morseCodeTypeSwitchLabel,
                    morseCodeOutput
                }
            };
            #endregion
        }

        #region Flash Morse Code
#if false
        private void FlashOn()
        /*
         * Flashlight flash on -- not tested
         */
        {
            /*
            Debug.WriteLine("FlashON!!!");
            var device = AVCaptureDevice.GetDefaultDevice(AVMediaType.Video);
            if (device == null)
            {
                return;
            }

            NSError error = null;
            device.LockForConfiguration(out error);
            if (error != null)
            {
                Debug.WriteLine(error);
                device.UnlockForConfiguration();
                return;
            }
            else
            {
                Debug.WriteLine("else initialed");
                if (device.TorchMode != AVCaptureTorchMode.On)
                {
                    Debug.WriteLine("Flashlight should be on");
                    device.TorchMode = AVCaptureTorchMode.On;
                }
                device.UnlockForConfiguration();
            }
            */
            // /*
            // AVCaptureDevice[] devices = AVCaptureDevice.DevicesWithMediaType(AVMediaType.Video); 
            // for (int i = 0; i < devices.Length; i++)
            Foundation.NSError error;
            for (int i = 0; i < AVCaptureDevice.Devices.Length; i++)
            {
                Debug.WriteLine("Has Device!!!");
                // AVCaptureDevice device = devices[i];                 
                AVCaptureDevice device = AVCaptureDevice.Devices[i];
                if (device.HasFlash == true)
                {
                    Debug.WriteLine("hasFlash!!!");
                    device.LockForConfiguration(out error);
                    device.TorchMode = AVCaptureTorchMode.On;
                    device.UnlockForConfiguration();
                }
            }
            //*/
        }

        private void FlashOff()
        /*
         * Flashlight flash off -- not tested
         */
        {
            // AVCaptureDevice[] devices = AVCaptureDevice.DevicesWithMediaType(AVMediaType.Video); 
            // for (int i = 0; i < devices.Length; i++)
            Foundation.NSError error;
            for (int i = 0; i < AVCaptureDevice.Devices.Length; i++)
            {
                // AVCaptureDevice device = devices[i];                 
                AVCaptureDevice device = AVCaptureDevice.Devices[i];
                if (device.HasFlash == true)
                {
                    device.LockForConfiguration(out error);
                    device.TorchMode = AVCaptureTorchMode.Off;
                    device.UnlockForConfiguration();
                }
            }
        }

        async private void FlashMorseCode(string text)
        /*
         * Uses the flashlight for morse code 
         */
        {
            // This function is now running
            running = true;

            // Converts text to all lowercase
            text = text.ToLower();

            foreach (char c in text)
            {
                string morseCode;
                if (!morseCodeDict.TryGetValue(c.ToString(), out morseCode))
                {
                    // The key isn't in the dictionary.
                    morseCode = "2";
                }

                foreach (char chr in morseCode)
                /*
             * Iterate over the morse code for a particular character
             */
                {
                    if (chr == '0')
                    {
                        // Flashlight flash
                        FlashOn();
                        var timeToSleep = (int)(100 / morseCodeSpeed);
                        await Task.Delay(timeToSleep); // Wait default .1 seconds without blocking
                        FlashOff();
                    }
                    else if (chr == '1')
                    {
                        // Flashlight flash
                        FlashOn();
                        var timeToSleep = (int)(300 / morseCodeSpeed);
                        await Task.Delay(timeToSleep); // Wait default .3 seconds without blocking
                        FlashOff();
                    }
                    else
                    /* 
                 * The letter space: " "
                 */
                    {
                        // Time to sleep between words, combines with time between letters to make default .7 sec delay
                        var timeToSleep = (int)(400 / morseCodeSpeed);
                        await Task.Delay(timeToSleep); // Wait default .4 seconds without blocking
                    }
                }

                // Time to sleep between letters
                var timeToSleepBetweenLeters = (int)(300 / morseCodeSpeed);
                await Task.Delay(timeToSleepBetweenLeters); // Wait default .3 seconds without blocking
            }

            // This function has completed
            running = false;
        }
#endif
        #endregion

        #region OnReturn(string text)
        private void OnReturn(string text)
        {
            DisplayMorseCode(text);

            if (morseCodeTypeSwitch.IsToggled)
            /*
             * Uses torch for Morse Code
             */
            {
                // FlashMorseCode(text);
            }
            else
            /*
             * Uses audio for Morse Code
             */
            {
                SoundMorseCode(text);
            }
        }
        #endregion

        #region SoundMorseCode
        async private void SoundMorseCode(string text)
        /* 
         * Uses audio file to output morse code
         * Combine this and FlashMorseCode into one function to remove duplicate code
         */
        {
            // This function is now running
            running = true;

            // Delay to accomadate startup lag (HAPPENS ON SIMULATOR, COMMENT OUT AND TEST FOR REAL DEVICE)
            await Task.Delay(100); // Wait .1 seconds

            // Colour the Morse Code character that is currently playing
            int range = 0;

            //var currentCharacter = new Forma
            //{
            //    ForegroundColor = UIColor.Red
            //};

            //var prettystring = new NSMutableAttributedString("");

            // Convert text to lowercase
            text = text.ToLower();

            // Reset cancel button if it was previously pressed
            if (cancel)
            {
                cancel = false;
            }

            foreach (char c in text)
            /*
             * Iterate over characters in string text
             */
            {
                string morseCode;
                if (!morseCodeDict.TryGetValue(c.ToString(), out morseCode))
                {
                    // The key isn't in the dictionary.
                    morseCode = "2";
                }

                foreach (char chr in morseCode)
                /*
                 * Iterate over the morse code for a particular character
                 */
                {
                    if (!cancel)
                    {
                        if (chr == '0')
                        {
                            // Play sound effect
                            //morseCodeBeep.PlaySystemSound();
                            normalTextBegin += highlightedText;
                            if (normalTextEnd.Length > 0)
                            {
                                highlightedText = normalTextEnd[0].ToString();
                                normalTextEnd = normalTextEnd.Substring(1);
                            }
                            morseCodeOutput.FormattedText = @morseCodeOutputString;
                            var timeToSleep = (int)(100 / morseCodeSpeed);
                            await Task.Delay(timeToSleep); // Wait default .1 seconds without blocking
                        }
                        else if (chr == '1')
                        {
                            // Play sound effect
                            //morseCodeBeep.PlaySystemSound();
                            normalTextBegin += highlightedText;
                            if (normalTextEnd.Length > 0)
                            {
                                highlightedText = normalTextEnd[0].ToString();
                                normalTextEnd = normalTextEnd.Substring(1);
                            }
                            morseCodeOutput.FormattedText = @morseCodeOutputString;
                            var timeToSleep = (int)(300 / morseCodeSpeed);
                            await Task.Delay(timeToSleep); // Wait default .3 seconds without blocking
                        }
                        else
                        /*
                         * The letter space: " "
                         */
                        {
                            // Accounts for double spaced spaces
                            range++;
                            // Time to sleep between words, combines with time between letters to make default .7 sec delay
                            var timeToSleep = (int)(400 / morseCodeSpeed);
                            await Task.Delay(timeToSleep); // Wait default .4 seconds without blocking
                        }
                        range++;
                    }
                    else
                    {
                        return;
                    }
                }

                // Time to sleep between letters
                var timeToSleepBetweenLeters = (int)(300 / morseCodeSpeed);
                await Task.Delay(timeToSleepBetweenLeters); // Wait default .3 seconds without blocking
            }

            // This function has completed
            running = false;
        }
        #endregion

        #region DisplayMorseCode
        private void DisplayMorseCode(string text)
        /* 
         * Displays Morse Code translation on the screen
         */
        {
            // Convert text to lowercase
            text = text.ToLower();

            // Reset output text
            normalTextEnd = "";
            normalTextBegin = "";
            highlightedText = "";

            foreach (char c in text)
            /*
             * Iterate over characters in string text
             */
            {
                string morseCode;
                if (!morseCodeDict.TryGetValue(c.ToString(), out morseCode))
                {
                    // The key isn't in the dictionary.
                    morseCode = "2";
                }

                foreach (char chr in morseCode)
                /*
                 * Iterate over the morse code for a particular character
                 */
                {
                    if (chr == '0')
                    {
                        normalTextEnd += "•";
                    }
                    else if (chr == '1')
                    {
                        normalTextEnd += "–";
                    }
                    else
                    /*
                     * The letter space: " "
                     */
                    {
                        // Two spaces for visual clarity
                        normalTextEnd += "  ";
                    }
                }
            }

            // Sets the output text to the Morse Code translation
            morseCodeOutput.FormattedText = @morseCodeOutputString;
        }
        #endregion

        #region HandleMorseCodeTextField[Input]
        private bool HandleMorseCodeTextFieldInput()
        /* 
         * Handles the morseCodeTextField input validation
         */
        {
            if (this.morseCodeTextField.Text.Length <= 0)
            /*
             * Perform a simple "required" validation
             */
            {
                //InvokeOnMainThread(() =>
                /*
                 * Need to update on the main thread to change the border color
                 */
                //{
                morseCodeTextField.BackgroundColor = Color.LightPink;
                morseCodeTextField.TextColor = Color.Red;
                //});
                return false;
            }
            else
            /*
             * Restore "ok" textfield settings
             */
            {
                //InvokeOnMainThread(() =>
                /*
                 * Need to update on the main thread to change the border color
                 */
                //{
                morseCodeTextField.BackgroundColor = Color.White;
                morseCodeTextField.TextColor = Color.Black;
                //});
                return true;
            }
        }

        private void HandleMorseCodeTextField(object sender, EventArgs e)
        /* 
         * Handles the keyboard RETURN button pressed for the morseCodeTextField
         */
        {

            if (HandleMorseCodeTextFieldInput() && !running)
            {
                var text = morseCodeTextField.Text;
                OnReturn(text);
            }
            else
            {
                // Add no text inputted error handling
                return;
            }
            return;
        }
        #endregion

        #region HandleMorseCodeSlider
        private void HandleMorseCodeSpeedSliderValueChanged(object sender, EventArgs e)
        {
            morseCodeSpeed = Math.Round(morseCodeSpeedSlider.Value, 1, MidpointRounding.AwayFromZero);
            morseCodeSpeedSlider.Value = (float)morseCodeSpeed;
            morseCodeSpeedSliderLabel.Text = $"Morse Code speed: {morseCodeSpeed}";
        }
        #endregion

        #region HandleCancelButtonPress
        private void HandleCancelButtonPress(object sender, EventArgs e)
        /*
         * Handles the UI cancelButton when pressed
         */
        {
            cancel = true;
            running = false;
        }
        #endregion

        #region HandleReturnButtonPress
        private void HandleReturnButtonPress(object sender, EventArgs e)
        /*
         * Handles the UI returnButton when pressed
         */
        {
            if (HandleMorseCodeTextFieldInput() && !running)
            {
                var text = morseCodeTextField.Text;
                OnReturn(text);
            }
            else
            {
                return;
            }
        }
        #endregion
    }
}
