#! usr/bin/python3
# -*- coding: utf-8 -*-

# Exit application
from sys import exit
# Base pygame
import pygame
from pygame import draw
# For keypresses
from pygame.locals import *
# Sleep between points
from time import sleep
# Randomness
from random import random, choice
from time import time
from math import ceil, floor

# TODO
# DONE # AI Player 2 (choice between single or double player)
# SEMI-DONE # Menu
# DONE # Pause
# DONE # Ball hitting edge of paddle sends the ball a predicted direction
# Multiple game modes / difficulties
# ... Ball speeds up faster / never speeds up
# ... AI is easy / hard


class PongRL:
    def __init__(self):
        pygame.init()
        self.fps = pygame.time.Clock()
        self.white = (255, 255, 255)
        self.backgroundColour = (0, 0, 0)
        self.width = 800
        self.height = 400
        self.ballRadius = 6
        self.scoreL = 0
        self.scoreR = 0
        self.font = pygame.font.SysFont("helvetica", 20)
        self.score = 0
        self.smoothMoveCounter = 0

        self.negativePaddleVelocity = -13
        self.positivePaddleVelocity = 13
        self.ballPosition = [0, 0]
        self.ballVelocity = [0, 0]
        self.paddleWidth = 8
        self.paddleHeight = 80
        self.halfPaddleWidth = self.paddleWidth // 2
        self.halfPaddleHeight = self.paddleHeight // 2
        self.paddleVelocityL = 0
        self.paddleVelocityR = 0
        self.paddlePositionL = [self.halfPaddleWidth, self.height // 2]
        self.paddlePositionR = [self.width - self.halfPaddleWidth, self.height // 2]

        # Initiates the window
        self.window = pygame.display.set_mode((self.width, self.height), 0, 32)
        # Sets the window title
        pygame.display.set_caption('PongRL Training')

        self.scaled_surface = pygame.Surface((84, 84), depth=32)
        # Start the game
        # self.wrapper()
        self.ballInit()

    def renderFirstFrame(self):
        # Draw screen
        self.drawBackground()

        # Updates both paddle's positions
        self.updatePaddlePosition()

        # Update position of the ball
        self.updateBallPosition()

        # Draws foreground elements
        self.drawForeground()

        # Updates scores
        self.updateScores()

        # RL
        pygame.transform.scale(pygame.display.get_surface(), (84, 84), self.scaled_surface)
        image_data = pygame.surfarray.array2d(self.scaled_surface)
        # return our surface data
        return image_data

    def renderNextFrame(self, action=None):
        # Update AI paddle
        self.AI(action)

        # Draw screen
        self.drawBackground()

        # Updates both paddle's positions
        self.updatePaddlePosition()

        # Update position of the ball
        self.updateBallPosition()

        # Draws foreground elements
        self.drawForeground()

        # Checks for object collision
        self.checkForCollision()

        # Updates scores
        self.updateScores()

        pygame.transform.scale(pygame.display.get_surface(), (84, 84), self.scaled_surface)
        image_data = pygame.surfarray.array2d(self.scaled_surface)
        # return our surface data
        # update the window
        self.getEvent()

        pygame.display.update()
        self.fps.tick(120)
        # record the total score
        # self.tally = self.tally + score
        # print("Tally is " + str(self.tally))
        # return the score and the surface data
        score = self.score
        self.score = 0
        return [score, image_data]

    def ballInit(self):
        # Ball starts at the middle of the screen
        self.ballPosition = [self.width // 2, self.height // 2]
        # Choose intial horizontal and vertical velocity for the ball
        horz = choice([-8, -6, -4, 4, 6, 8])
        vert = choice([-1, 1])
        # Sets the ball's velocity
        self.ballVelocity = [horz, vert]

    def AI(self, action):
        # if self.smoothMoveCounter == 0:
        #    self.reaction = action
        if self.smoothMoveCounter < 4:
            self.smoothMoveCounter += 1
        else:
            self.smoothMoveCounter = 0
            if (action[1] == 1):
                self.paddleVelocityR = -4
            elif (action[2] == 1):
                self.paddleVelocityR = 4
            else:
                self.paddleVelocityR = 0

    def getEvent(self):
        for event in pygame.event.get():
            # Key is pressed down
            if event.type == KEYDOWN:
                self.keydown(event)
            # Key press down is lifted
            elif event.type == KEYUP:
                self.keyup(event)
            elif event.type == QUIT:
                pygame.quit()
                exit()

    def updatePaddlePosition(self):
        ###
        # Update left paddle's vertical position, keeping paddle on the screen
        # ... Paddle is in a acceptable range
        ###

        # Left paddle
        if (
            self.paddlePositionL[1] > self.halfPaddleHeight
        ) and (
            self.paddlePositionL[1] < self.height - self.halfPaddleHeight
        ):
            self.paddlePositionL[1] += self.paddleVelocityL
        # So paddle doesn't get stuck at the top of the screen
        elif (
            self.paddlePositionL[1] <= self.halfPaddleHeight
        ) and self.paddleVelocityL > 0:
            self.paddlePositionL[1] += self.paddleVelocityL
        # So paddle doesn't get stuck at the bottom of the screen
        elif (
            self.paddlePositionL[1] >= self.height - self.halfPaddleHeight
        ) and self.paddleVelocityL < 0:
            self.paddlePositionL[1] += self.paddleVelocityL

        # Right paddle
        if (
            self.paddlePositionR[1] > self.halfPaddleHeight
        ) and (
            self.paddlePositionR[1] < self.height - self.halfPaddleHeight
        ):
            self.paddlePositionR[1] += self.paddleVelocityR
        # So paddle doesn't get stuck at the top of the screen
        elif (
            self.paddlePositionR[1] <= self.halfPaddleHeight
        ) and self.paddleVelocityR > 0:
            self.paddlePositionR[1] += self.paddleVelocityR
        # So paddle doesn't get stuck at the bottom of the screen
        elif (
            self.paddlePositionR[1] >= self.height - self.halfPaddleHeight
        ) and self.paddleVelocityR < 0:
            self.paddlePositionR[1] += self.paddleVelocityR

    def drawBackground(self):
        # Draw screen
        self.window.fill(self.backgroundColour)
        draw.line(self.window, self.white, [self.width // 2, 0], [self.width // 2, self.height], 1)

    def updateBallPosition(self):
        # Update position of the ball
        self.ballPosition[0] += int(ceil(self.ballVelocity[0]))
        if self.ballVelocity[1] <= 0:
            self.ballPosition[1] += int(floor(self.ballVelocity[1]))
        else:
            self.ballPosition[1] += int(ceil(self.ballVelocity[1]))

    def drawForeground(self):
        # Draw ball
        draw.circle(self.window, self.white, self.ballPosition, self.ballRadius, 0)

        # Draw left paddle
        draw.polygon(self.window,
                     self.white,
                     [[self.paddlePositionL[0] - self.halfPaddleWidth,
                      self.paddlePositionL[1] - self.halfPaddleHeight],
                      [self.paddlePositionL[0] - self.halfPaddleWidth,
                      self.paddlePositionL[1] + self.halfPaddleHeight],
                      [self.paddlePositionL[0] + self.halfPaddleWidth,
                      self.paddlePositionL[1] + self.halfPaddleHeight],
                      [self.paddlePositionL[0] + self.halfPaddleWidth,
                      self.paddlePositionL[1] - self.halfPaddleHeight]],
                     0)

        # Draw right paddle
        draw.polygon(self.window,
                     self.white,
                     [[self.paddlePositionR[0] - self.halfPaddleWidth,
                      self.paddlePositionR[1] - self.halfPaddleHeight],
                      [self.paddlePositionR[0] - self.halfPaddleWidth,
                      self.paddlePositionR[1] + self.halfPaddleHeight],
                      [self.paddlePositionR[0] + self.halfPaddleWidth,
                      self.paddlePositionR[1] + self.halfPaddleHeight],
                      [self.paddlePositionR[0] + self.halfPaddleWidth,
                      self.paddlePositionR[1] - self.halfPaddleHeight]],
                     0)

    def checkForCollision(self):
        # Ball collision with top wall
        if (self.ballPosition[1]) <= self.ballRadius:
            # Ball bounces off top wall
            self.ballVelocity[1] = -self.ballVelocity[1]
        # Ball collision with bottom wall
        elif (self.ballPosition[1]) >= self.height - self.ballRadius:
            # Ball bounces off bottom wall
            self.ballVelocity[1] = -self.ballVelocity[1]

        # Collision with left paddle
        if (
            self.ballPosition[0]
        ) in range(
            -self.ballRadius - self.paddleWidth, self.ballRadius + self.paddleWidth
        ) and (
            self.ballPosition[1]
        ) in range(
            self.paddlePositionL[1] - self.halfPaddleHeight, self.paddlePositionL[1] + self.halfPaddleHeight, 1
        ) and self.ballVelocity[0] < 0:
            # Ball bouncing off paddle
            self.ballVelocity[0] = -self.ballVelocity[0]
            # Increase ball's speed for a greater challange
            self.ballVelocity[0] *= 1.00
            # Vertical velocity
            origHeight = self.paddlePositionL[1]
            ballDelta = origHeight - self.ballPosition[1]
            # Ball going downward with a positive y velocity
            if self.ballVelocity[1] > 0:
                self.ballVelocity[1] += (-ballDelta) / 20
            # Ball going upward has a negative y velocity
            else:
                self.ballVelocity[1] -= (ballDelta) / 20
        # Collision with gutter
        elif (self.ballPosition[0]) <= self.ballRadius - self.paddleWidth:
            # Since this is training the AI,
            # ... The player can never lose,
            # ... so the ball stays in play more often
            # ... It's like the computer is playing against itself
            self.ballVelocity[0] = -self.ballVelocity[0]
            # Increase ball's speed for a greater challange
            self.ballVelocity[0] *= 1.00
            # Vertical velocity
            origHeight = self.paddlePositionL[1]
            ballDelta = origHeight - self.ballPosition[1]
            # Ball going upward has a negative y velocity
            if self.ballVelocity[1] > 0:
                self.ballVelocity[1] += (-ballDelta) / 200
            else:
                self.ballVelocity[1] -= (ballDelta) / 200
            self.scoreR += 1
            # Delay between points
            # self.renderCountdown()
            # Right player won the point
            # Next point is initiated
            # self.ballInit()

        # Collision with right paddle
        if (
            self.ballPosition[0]
        ) in range(
            self.width - self.ballRadius - self.paddleWidth, self.width + self.ballRadius + self.paddleWidth
        ) and (
            self.ballPosition[1]
        ) in range(
            self.paddlePositionR[1] - self.halfPaddleHeight, self.paddlePositionR[1] + self.halfPaddleHeight, 1
        ) and self.ballVelocity[0] > 0:
            # Ball bouncing off paddle
            self.ballVelocity[0] = -self.ballVelocity[0]
            # Increase ball's speed for a greater challange
            self.ballVelocity[0] *= 1.00
            # Determins steepness of vertical velocity based on
            # ... where the ball hits the paddle
            origHeight = self.paddlePositionR[1]
            ballDelta = origHeight - self.ballPosition[1]
            # Ball going upward has a negative y velocity
            if self.ballVelocity[1] > 0:
                self.ballVelocity[1] += (-ballDelta) / 20
            else:
                self.ballVelocity[1] -= (ballDelta) / 20
            # Positive reinforcement for hitting the paddle,
            # ... More points for hitting the centre of the paddle
            # self.score += ceil(30 / abs(ballDelta + .1))
            self.score += 10000 / abs(ballDelta + .1)
        # Collision with gutter
        elif (self.ballPosition[0]) >= self.width - self.ballRadius + self.paddleWidth:
            self.ballVelocity[0] = -self.ballVelocity[0]
            # Increase ball's speed for a greater challange
            self.ballVelocity[0] *= 1.00
            # Determins the harshness of the penalty
            origHeight = self.paddlePositionR[1]
            ballDelta = abs(origHeight - self.ballPosition[1])
            # Penalty for missing the ball
            # Bigger penalty for missing the ball by a larger amount
            self.score -= ballDelta * 10
            # Left player won the point
            self.scoreL += 1
            # Next point is initiated
            self.ballInit()

    def updateScores(self):
        # Update scores
        scoreLeft = self.font.render(str(self.scoreL), 1, self.white)
        self.window.blit(scoreLeft, (self.width // 2 - 65, 20))

        scoreRight = self.font.render(str(self.scoreR), 1, self.white)
        self.window.blit(scoreRight, (self.width // 2 + 50, 20))

    def keydown(self, event):
        # Keydown handlerp
        if event.key == K_w:
            self.paddleVelocityL = self.negativePaddleVelocity
        elif event.key == K_s:
            self.paddleVelocityL = self.positivePaddleVelocity

    def keyup(self, event):
        # Keyup handler
        if event.key in (K_w, K_s):
            self.paddleVelocityL = 0


class Pong:
    def __init__(self):
        pygame.init()
        self.fps = pygame.time.Clock()
        self.white = (255, 255, 255)
        self.backgroundColour = (0, 0, 0)
        self.width = 1200
        self.height = 400
        self.ballRadius = 6
        self.scoreL = 0
        self.scoreR = 0
        self.font = pygame.font.SysFont("helvetica", 20)
        self.titleFont = pygame.font.SysFont("helvetica", 40)

        self.negativePaddleVelocity = -3
        self.positivePaddleVelocity = 3
        self.ballPosition = [0, 0]
        self.ballVelocity = [0, 0]
        self.paddleWidth = 8
        self.paddleHeight = 80
        self.halfPaddleWidth = self.paddleWidth // 2
        self.halfPaddleHeight = self.paddleHeight // 2
        self.paddleVelocityL = 0
        self.paddleVelocityR = 0
        self.paddlePositionL = [self.halfPaddleWidth, self.height // 2]
        self.paddlePositionR = [self.width - self.halfPaddleWidth, self.height // 2]
        self.running = 0
        self.paused = 1
        self.menu = 2
        self.startMenu = 3
        self.runningSinglePlayer = 4
        self.runningMultiPlayer = 5
        self.state = self.startMenu
        self.pauseScreen = self.font.render('Paused', 1, self.white)

        # Initiates the window
        self.window = pygame.display.set_mode((self.width, self.height), 0, 32)
        # Sets the window title
        pygame.display.set_caption('Pong')

    def ballInit(self):
        # Ball starts at the middle of the screen
        self.ballPosition = [self.width // 2, self.height // 2]
        # Choose intial horizontal and vertical velocity for the ball
        horz = choice([-4, -3, -2, 2, 3, 4])
        vert = choice([-1, 1])
        # Sets the ball's velocity
        self.ballVelocity = [horz, vert]

    def AI(self):
        # Machine Learning
        # Input
        # ... ballPosition
        # ... ballVelocity
        # ... paddlePositionR
        # Output
        # ... Move up or down
        # More complicated
        # ... Adjust paddle height to make abs(ball velocity) higher so opponent loses more
        #
        # Ball position x
        # Ball above or below paddle
        # Ball velocity y, 1 for up, 0 for down
        if self.ballPosition[0] < self.width / 2:
            ballPositionX = 0
        else:
            ballPositionX = 1
        if self.ballPosition[1] > self.paddlePositionR[1]:
            ballAbove = 1
        else:
            ballAbove = 0
        if self.ballVelocity[1] < 0:
            velocity = 1
        else:
            velocity = 0

        # upOrDown = clf.predict([[ballPositionX, ballAbove, velocity]])
        if ballAbove == 1:  # upOrDown[0] == 1:
            self.paddleVelocityR = 3
        else:
            self.paddleVelocityR = -3

    def updatePaddlePosition(self):
        # Update left paddle's vertical position, keeping paddle on the screen
        # Paddle is in a acceptable range
        if self.paddlePositionL[1] > self.halfPaddleHeight and self.paddlePositionL[1] < self.height - self.halfPaddleHeight:
            self.paddlePositionL[1] += self.paddleVelocityL
        # So paddle doesn't get stuck at the top of the screen
        elif self.paddlePositionL[1] <= self.halfPaddleHeight and self.paddleVelocityL > 0:
            self.paddlePositionL[1] += self.paddleVelocityL
        # So paddle doesn't get stuck at the bottom of the screen
        elif self.paddlePositionL[1] >= self.height - self.halfPaddleHeight and self.paddleVelocityL < 0:
            self.paddlePositionL[1] += self.paddleVelocityL

        if self.state == self.runningMultiPlayer:
            pass
        else:
            self.AI()

        # Update right paddle's vertical position, keeping paddle on the screen
        # Paddle is in a acceptable range
        if self.paddlePositionR[1] > self.halfPaddleHeight and self.paddlePositionR[1] < self.height - self.halfPaddleHeight:
            self.paddlePositionR[1] += self.paddleVelocityR
        # So paddle doesn't get stuck at the top of the screen
        elif self.paddlePositionR[1] <= self.halfPaddleHeight and self.paddleVelocityR > 0:
            self.paddlePositionR[1] += self.paddleVelocityR
        # So paddle doesn't get stuck at the bottom of the screen
        elif self.paddlePositionR[1] >= self.height - self.halfPaddleHeight and self.paddleVelocityR < 0:
            self.paddlePositionR[1] += self.paddleVelocityR

    def drawBackground(self):
        ###
        # Draws the background colour as well as any stationary elements
        ###
        # Draw screen
        self.window.fill(self.backgroundColour)
        draw.line(self.window, self.white, [self.width // 2, 0], [self.width // 2, self.height], 1)

    def updateBallPosition(self):
        # Update position of the ball
        self.ballPosition[0] += int(ceil(self.ballVelocity[0]))
        if self.ballVelocity[1] <= 0:
            self.ballPosition[1] += int(floor(self.ballVelocity[1]))
        else:
            self.ballPosition[1] += int(ceil(self.ballVelocity[1]))

    def drawForeground(self):
        ###
        # Draws moving components
        ###
        # Draw ball
        draw.circle(self.window, self.white, self.ballPosition, self.ballRadius, 0)

        # Draw left paddle
        draw.polygon(self.window,
                     self.white,
                     [[self.paddlePositionL[0] - self.halfPaddleWidth,
                      self.paddlePositionL[1] - self.halfPaddleHeight],
                      [self.paddlePositionL[0] - self.halfPaddleWidth,
                      self.paddlePositionL[1] + self.halfPaddleHeight],
                      [self.paddlePositionL[0] + self.halfPaddleWidth,
                      self.paddlePositionL[1] + self.halfPaddleHeight],
                      [self.paddlePositionL[0] + self.halfPaddleWidth,
                      self.paddlePositionL[1] - self.halfPaddleHeight]],
                     0)

        # Draw right paddle
        draw.polygon(self.window,
                     self.white,
                     [[self.paddlePositionR[0] - self.halfPaddleWidth,
                      self.paddlePositionR[1] - self.halfPaddleHeight],
                      [self.paddlePositionR[0] - self.halfPaddleWidth,
                      self.paddlePositionR[1] + self.halfPaddleHeight],
                      [self.paddlePositionR[0] + self.halfPaddleWidth,
                      self.paddlePositionR[1] + self.halfPaddleHeight],
                      [self.paddlePositionR[0] + self.halfPaddleWidth,
                      self.paddlePositionR[1] - self.halfPaddleHeight]],
                     0)

    def checkForCollision(self):
        ###
        # Checks for object collisions
        ###
        # Ball collision with top wall
        if (self.ballPosition[1]) <= self.ballRadius:
            # Ball bounces off top wall
            self.ballVelocity[1] = -self.ballVelocity[1]
        # Ball collision with bottom wall
        elif (self.ballPosition[1]) >= self.height - self.ballRadius:
            # Ball bounces off bottom wall
            self.ballVelocity[1] = -self.ballVelocity[1]

        # Collision with left paddle
        if (self.ballPosition[0]) in range(-self.ballRadius - self.paddleWidth, self.ballRadius + self.paddleWidth) and (self.ballPosition[1]) in range(self.paddlePositionL[1] - self.halfPaddleHeight, self.paddlePositionL[1] + self.halfPaddleHeight, 1) and self.ballVelocity[0] < 0:
            # Ball bouncing off paddle
            self.ballVelocity[0] = -self.ballVelocity[0]
            # Increase ball's speed for a greater challange
            self.ballVelocity[0] *= 1.00
            # Vertical velocity
            origHeight = self.paddlePositionL[1]
            ballDelta = origHeight - self.ballPosition[1]
            # Ball going upward has a negative y velocity
            if self.ballVelocity[1] > 0:
                self.ballVelocity[1] += ballDelta / 20
            else:
                self.ballVelocity[1] -= ballDelta / 20
        # Collision with gutter
        elif (self.ballPosition[0]) <= self.ballRadius - self.paddleWidth:
            # Delay between points
            self.renderCountdown()
            # Right player won the point
            self.scoreR += 1
            # Next point is initiated
            self.ballInit()

        # Collision with right paddle
        if (self.ballPosition[0]) in range(self.width - self.ballRadius - self.paddleWidth, self.width + self.ballRadius + self.paddleWidth) and (self.ballPosition[1]) in range(self.paddlePositionR[1] - self.halfPaddleHeight, self.paddlePositionR[1] + self.halfPaddleHeight, 1) and self.ballVelocity[0] > 0:
            # Ball bouncing off paddle
            self.ballVelocity[0] = -self.ballVelocity[0]
            # Increase ball's speed for a greater challange
            self.ballVelocity[0] *= 1.00
            # Vertical velocity
            origHeight = self.paddlePositionR[1]
            ballDelta = origHeight - self.ballPosition[1]
            # Ball going upward has a negative y velocity
            if self.ballVelocity[1] > 0:
                self.ballVelocity[1] += ballDelta / 20
            else:
                self.ballVelocity[1] -= ballDelta / 20
        # Collision with gutter
        elif (self.ballPosition[0]) >= self.width - self.ballRadius + self.paddleWidth:
            # Delay between points
            self.renderCountdown()
            # Left player won the point
            self.scoreL += 1
            # Next point is initiated
            self.ballInit()

    def updateScores(self):
        # Update scores
        scoreLeft = self.font.render(str(self.scoreL), 1, self.white)
        self.window.blit(scoreLeft, (self.width // 2 - 65, 20))

        scoreRight = self.font.render(str(self.scoreR), 1, self.white)
        self.window.blit(scoreRight, (self.width // 2 + 50, 20))

    def renderNextFrame(self):
        # self.AI()

        # Draw screen
        self.drawBackground()

        # Updates both paddle's positions
        self.updatePaddlePosition()

        # Update position of the ball
        self.updateBallPosition()

        # Draws foreground elements
        self.drawForeground()

        # Checks for object collision
        self.checkForCollision()

        # Updates scores
        self.updateScores()

    def renderPausedScreen(self):
        self.window.blit(self.pauseScreen, (self.width // 2 - 35, self.height // 2))

    def render(self):
        # self.state is 0, 4, 5 when running, 1 when paused
        if self.state in [0, 4, 5]:
            self.renderNextFrame()
        elif self.state == 1:
            self.renderPausedScreen()

    def keydown(self, event):
        # Keydown handlerp
        if event.key == K_UP:
            self.paddleVelocityR = self.negativePaddleVelocity
        elif event.key == K_DOWN:
            self.paddleVelocityR = self.positivePaddleVelocity
        elif event.key == K_w:
            self.paddleVelocityL = self.negativePaddleVelocity
        elif event.key == K_s:
            self.paddleVelocityL = self.positivePaddleVelocity
        elif event.key == K_p:
            # Paused is 1
            self.state = self.running if self.state == 1 else self.paused

    def keyup(self, event):
        # Keyup handler
        if event.key in (K_w, K_s):
            self.paddleVelocityL = 0
        elif event.key in (K_UP, K_DOWN):
            self.paddleVelocityR = 0

    def keydownMenu(self, event):
        # Keydown handlerp
        if event.key == K_UP:
            pass
        elif event.key == K_DOWN:
            pass
        elif event.key == K_1:
            self.running = self.runningSinglePlayer
            self.state = self.runningSinglePlayer
        elif event.key == K_2:
            self.running = self.runningMultiPlayer
            self.state = self.runningMultiPlayer
        elif event.key == K_RETURN:
            self.state = self.runningMultiPlayer

    def keyupMenu(self, event):
        # Keyup handler
        pass

    def displayStartMenu(self):
        singlePlayer = self.font.render('Press `1` to start single player', 1, self.white)
        multiPlayer = self.font.render('Press `2` to start multi player', 1, self.white)
        self.window.blit(singlePlayer, (self.width // 2 - 120, self.height // 2 + 12))
        self.window.blit(multiPlayer, (self.width // 2 - 120, self.height // 2 - 12))

    def renderStartMenu(self):
        while self.state == 3:
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    self.keydownMenu(event)
                # Key press down is lifted
                elif event.type == KEYUP:
                    self.keyupMenu(event)
                elif event.type == QUIT:
                    pygame.quit()
                    exit()

            self.displayStartMenu()
            self.fps.tick(15)

            pygame.display.update()

    def renderFreezeFrame(self):
        ###
        # Used to render the background for the countdown
        ###
        # Draw screen
        self.drawBackground()

        # Draws foreground elements
        self.drawForeground()

    def renderCountdown(self):
        two = self.titleFont.render('2', 1, self.white)
        one = self.titleFont.render('1', 1, self.white)
        three = self.titleFont.render('3', 1, self.white)
        rightNow = time()
        self.renderFreezeFrame()
        while time() - rightNow <= 1:
            self.window.blit(three, (self.width // 2 - 35, self.height // 2))
            pygame.display.update()
        self.renderFreezeFrame()
        while time() - rightNow <= 2:
            self.window.blit(two, (self.width // 2 - 35, self.height // 2))
            pygame.display.update()
        self.renderFreezeFrame()
        while time() - rightNow <= 3:
            self.window.blit(one, (self.width // 2 - 35, self.height // 2))
            pygame.display.update()

    def wrapper(self):
        ###
        # Controls the main flow of the program
        ###
        self.renderStartMenu()

        # Countdown from pressing menu start to the game starting: 3...2...1...[GO!]
        self.renderCountdown()

        # Initial point
        self.ballInit()

        # Game loop
        while True:
            self.render()
            for event in pygame.event.get():
                # Key is pressed down
                if event.type == KEYDOWN:
                    self.keydown(event)
                # Key press down is lifted
                elif event.type == KEYUP:
                    self.keyup(event)
                elif event.type == QUIT:
                    pygame.quit()
                    exit()

            pygame.display.update()
            self.fps.tick(120)


if __name__ == '__main__':
    Pong().wrapper()
