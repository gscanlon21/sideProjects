from os import environ
# Tenasorflow ^1.2 has no GPU support for MAC OSX
import tensorflow as tf
import pong
# Math
import numpy as np
# Random
import random
# Queue data structure
from collections import deque
# Hide warnings
environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

# Credits:
# Code for RL origionally from:
# https://github.com/malreddysid/pong_RL
# Someone else documented and wrapped their code ^ which is easier to understand:
# https://github.com/llSourcell/pong_neural_network_live
#
# I merely tied in their RL code into my pong program

# Hyper params
ACTIONS = 3  # up, down, stay
# Define our learning rate
GAMMA = 0.99
# For updating our gradient or training over time
INITIAL_EPSILON = 1.0
FINAL_EPSILON = 0.05
# How many frames to anneal epsilon
EXPLORE = 500000
OBSERVE = 50000
# Store our experiences, the size of it
REPLAY_MEMORY = 500000
# Batch size to train on
BATCH = 100
# Input image size in pixels
INPUT_SIZE = 84


# Create tensorflow graph
def createGraph():
    # CNN
    # Creates an empty tensor with all elements set to zero with a shape
    W_conv1 = tf.Variable(tf.random_normal([8, 8, 4, 32]))
    b_conv1 = tf.Variable(tf.random_normal([32]))

    W_conv2 = tf.Variable(tf.random_normal([4, 4, 32, 64]))
    b_conv2 = tf.Variable(tf.random_normal([64]))

    W_conv3 = tf.Variable(tf.random_normal([3, 3, 64, 64]))
    b_conv3 = tf.Variable(tf.random_normal([64]))

    # Image size 7x7 due to convolutions
    W_fc4 = tf.Variable(tf.random_normal([7 * 7 * 64, 784]))
    b_fc4 = tf.Variable(tf.random_normal([784]))

    W_fc5 = tf.Variable(tf.random_normal([784, ACTIONS]))
    b_fc5 = tf.Variable(tf.random_normal([ACTIONS]))

    # Input for pixel data
    s = tf.placeholder("float", [None, INPUT_SIZE, INPUT_SIZE, 4])

    # Computes rectified linear unit activation fucntion on a 2-D convolution
    # Given 4-D input and filter tensors.
    conv1 = tf.nn.relu(
        tf.nn.conv2d(s, W_conv1, strides=[1, 4, 4, 1], padding="VALID") + b_conv1)

    conv2 = tf.nn.relu(
        tf.nn.conv2d(conv1, W_conv2, strides=[1, 2, 2, 1], padding="VALID") + b_conv2)

    conv3 = tf.nn.relu(
        tf.nn.conv2d(conv2, W_conv3, strides=[1, 1, 1, 1], padding="VALID") + b_conv3)

    conv3_flat = tf.reshape(conv3, [-1, 7 * 7 * 64])

    fc4 = tf.nn.relu(tf.matmul(conv3_flat, W_fc4) + b_fc4)

    fc5 = tf.matmul(fc4, W_fc5) + b_fc5

    # Return input and output to the network
    return s, fc5


# Deep q network. feed in pixel data to graph session
def trainGraph(inp, out, sess):

    # To calculate the argmax, we multiply the predicted output with a vector
    # ... with one value 1 and rest as 0
    argmax = tf.placeholder("float", [None, ACTIONS])
    gt = tf.placeholder("float", [None])  # ground truth

    # Action
    action = tf.reduce_sum(tf.multiply(out, argmax), reduction_indices=1)
    # Cost function we will reduce through backpropagation
    cost = tf.reduce_mean(tf.square(action - gt))
    # Optimization function to reduce our minimize our cost function
    train_step = tf.train.AdamOptimizer(elipson=1e-6).minimize(cost)

    # Initialize our game
    game = pong.PongRL()

    # Create a queue for experience replay to store policies
    D = deque()

    # Intial frame
    frame = game.renderFirstFrame()
    # Convert rgb to gray scale for processing

    # Stack frames, that is our input tensor
    inp_t = np.stack((frame, frame, frame, frame), axis=2)

    # saver
    saver = tf.train.Saver()

    sess.run(tf.global_variables_initializer())

    t = 0
    epsilon = INITIAL_EPSILON

    # Training loop
    while(1):
        # Output tensor
        out_t = out.eval(feed_dict={inp: [inp_t]})[0]
        # Argmax function
        argmax_t = np.zeros([ACTIONS])

        # Random action with prob epsilon
        if(random.random() <= epsilon):
            maxIndex = random.randrange(ACTIONS)
        # Predicted action with prob (1 - epsilon)
        else:
            maxIndex = np.argmax(out_t)
        argmax_t[maxIndex] = 1

        if epsilon > FINAL_EPSILON:
            epsilon -= (INITIAL_EPSILON - FINAL_EPSILON) / EXPLORE

        # Reward tensor if score is positive
        reward_t, frame = game.renderNextFrame(argmax_t)

        frame = np.reshape(frame, (INPUT_SIZE, INPUT_SIZE, 1))

        # New input tensor
        inp_t1 = np.append(frame, inp_t[:, :, 0:3], axis=2)

        # Add our input tensor, argmax tensor, reward and updated input tensor
        # ... to stack of experiences
        D.append((inp_t, argmax_t, reward_t, inp_t1))

        # If we run out of replay memory, make room
        if len(D) > REPLAY_MEMORY:
            D.popleft()

        # Training iteration
        if t > OBSERVE:

            # get values from our replay memory
            minibatch = random.sample(D, BATCH)

            inp_batch = [d[0] for d in minibatch]
            argmax_batch = [d[1] for d in minibatch]
            reward_batch = [d[2] for d in minibatch]
            inp_t1_batch = [d[3] for d in minibatch]

            gt_batch = []
            out_batch = out.eval(feed_dict={inp: inp_t1_batch})

            # Add values to our batch
            for i in range(0, len(minibatch)):
                gt_batch.append(reward_batch[i] + GAMMA * np.max(out_batch[i]))

            # Train on that
            train_step.run(feed_dict={
                           gt: gt_batch,
                           argmax: argmax_batch,
                           inp: inp_batch
                           })

        # Update our input tensor to the next frame
        inp_t = inp_t1
        t = t + 1

        # Print our where we are after saving where we are
        if t % 10000 == 0:
            saver.save(sess, './pongDQN', global_step=t)

            print("TIMESTEP", t,
                  "/ EPSILON", epsilon,
                  "/ ACTION", maxIndex,
                  "/ REWARD", reward_t,
                  "/ Q_MAX %e" % np.max(out_t))


def main():
    # Create session
    sess = tf.InteractiveSession()
    # Input layer and output layer by creating graph
    inp, out = createGraph()
    # Train our graph on input and output with session variables
    trainGraph(inp, out, sess)


if __name__ == "__main__":
    main()
