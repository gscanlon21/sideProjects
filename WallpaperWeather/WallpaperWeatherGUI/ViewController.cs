﻿//
// ViewController.cs
//
// Author:
//       Graham Scanlon <gscanlon21@gmail.com>
//
// Copyright (c) 2017 Graham Scanlon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
using System;

using AppKit;
using Foundation;
// To create shapes (sa. rectangle)
using CoreGraphics;
// Debug.WriteLine('using System.Diagnostics');
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Net.Http;
using Contentful.Core;
using Newtonsoft.Json;
using System.Net;
using System.Linq;
using Newtonsoft.Json.Linq;


namespace WallpaperWeatherGUI
{
    public partial class ViewController : NSViewController
    {
        NSButton cancelButton;
        NSTextField weatherTextField;
        NSTextView currentActionTextView;

        private static readonly WebClient webClient = new WebClient();
        private static readonly HttpClient client = new HttpClient();
        private static readonly Random random = new Random();

        private static String zipcode = "63131";
        private static Boolean cancel = false;
        private static String resourcePath;

        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            #region base
            base.ViewDidLoad();
            base.Title = "Wallpaper Weather";

            string tempPath = Path.GetTempPath(); ;
            Directory.CreateDirectory($"{tempPath}/WallpaperWeatherGUI/Resources");
            resourcePath = $"{tempPath}/WallpaperWeatherGUI/Resources";
            var width = View.Bounds.Width;
            var height = View.Bounds.Height;
            Console.WriteLine($"{width},{height}");

            #endregion

            #region cancelButton
            cancelButton = new NSButton
            {
                Title = "Cancel?",
                Frame = new CGRect(20, 20, 100, 40), // x, y, width, height
            };
            cancelButton.SetButtonType(NSButtonType.Switch);
            cancelButton.Activated += HandleCancelButtonPress;
            View.AddSubview(cancelButton);
            #endregion

            #region weatherTextField
            nfloat heightSelf = 40;
            weatherTextField = new NSTextField
            {
                Editable = false,
                BackgroundColor = NSColor.LightGray,
                StringValue = "",
                Frame = new CGRect(20, height - 20 - heightSelf, 160, heightSelf), // x, y, width, height
            };
            View.AddSubview(weatherTextField);
            #endregion

            #region currentActionTextView
            // nfloat widthSelf = 220;

            currentActionTextView = new NSTextView
            {
                Editable = false,
                Selectable = false,
                Value = "",
                BackgroundColor = NSColor.White,
                AutoresizingMask = NSViewResizingMask.HeightSizable,
                MinSize = new CGSize(200, 133),
                MaxSize = new CGSize(300, 13333),
                Frame = new CGRect(0, 110, 238, 133), // x, y, width, height
            };
            currentActionTextView.TextDidChange += HandleCurrentActionTextViewTextDidChange;

            var clipView = new NSClipView
            {
                BackgroundColor = NSColor.White,
                Frame = new CGRect(1, 111, 238, 133), // x, y, width, height
                AutoresizingMask = NSViewResizingMask.HeightSizable,
                DocumentView = currentActionTextView,
            };

            var scrollView = new NSScrollView
            {
                BackgroundColor = NSColor.White,
                HasVerticalScroller = true,
                VerticalLineScroll = 10,
                VerticalPageScroll = 10,
                Frame = new CGRect(190, 110, 240, 135), // x, y, width, height
                ContentView = clipView,
            };

            View.AddSubview(scrollView);
            #endregion

            RequestWallpaper();
        }

        public override NSObject RepresentedObject
        {
            get
            {
                return base.RepresentedObject;
            }
            set
            {
                base.RepresentedObject = value;
            }
        }

        private async void RequestWallpaper()
        {
            while (!cancel)
            {
                string uri = $"https://api.aerisapi.com/forecasts/{zipcode}?client_id={Secrets.clientId}&client_secret={Secrets.clientSecret}";
                string responseForecast = await client.GetStringAsync($"{uri}");
                dynamic responseForecastJSON = JsonConvert.DeserializeObject(responseForecast);

                string weatherForecast = responseForecastJSON.response[0].periods[0].weather;
                weatherTextField.StringValue = $"{weatherForecast}";
                string weather = weatherForecast.ToLower();

                currentActionTextView.Value += "\nDownloading image and setting it to your wallpaper...\n";
                currentActionTextView.DidChangeText();

                var assets = $"https://cdn.contentful.com/spaces/{Secrets.spaceId}/assets?access_token={Secrets.contentDelivery}";
                String assetsResponse = await client.GetStringAsync($"{assets}");
                dynamic assetsResponseJSON = JsonConvert.DeserializeObject(assetsResponse);

                int total = (int)assetsResponseJSON.total;
                List<string> weatherMatchesList = new List<string>();

                for (int i = 0; i < total; i++)
                {
                    string description = assetsResponseJSON.items[i].fields.description;
                    List<string> descriptionTags = description.Split(',').Select(item => item.Trim()).ToList();
                    string fileUri = assetsResponseJSON.items[i].fields.file.url;

                    if (descriptionTags.Any(weather.Contains))
                    {
                        weatherMatchesList.Add(fileUri);
                    }
                }

                int randomIndex = random.Next(weatherMatchesList.Count());
                String randomFileUri = weatherMatchesList[randomIndex];
                String weatherPhoto = $"https:{randomFileUri}";

                // Random index appended to downloaded file name to fix an AppleScript hardship where setting
                // ... the background image to the same filename wouldn't work even if the file was different
                String downloadFilename = $"weatherPhoto{randomIndex}.jpg";
                String downloadFile = $"{resourcePath}/{downloadFilename}";
                // Download the image, blocks the thread while the download is in progress


                /* Commented out for faster and less distractions during UI modifications
                webClient.DownloadFile($"{weatherPhoto}", $"{downloadFile}");

                string path = $"{resourcePath}/setWallpaperScript.sh";
                string finder = "\"Finder\"";
                string photoFile = $"\"{downloadFile}\"";
                string command = $"osascript -e 'tell application {finder} to set desktop picture to {photoFile} as POSIX file'";
                string createText = "#!/bin/bash" + Environment.NewLine + command;
                File.WriteAllText(path, createText);
     
                Process proc = new System.Diagnostics.Process();
                proc.StartInfo.FileName = "/bin/bash";
                proc.StartInfo.Arguments = $"{path}";
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.Start();
                */

                // Setting image to wallpaper delay
                await Task.Delay(3000);

                currentActionTextView.Value += "\nAll operations completed successfully, will resume in an hour\n\n";
                currentActionTextView.DidChangeText();

                // Wait one hour before re-executing
                await Task.Delay(3600000);
            }
        }

        private void HandleCurrentActionTextViewTextDidChange(object sender, EventArgs e)
        {
            currentActionTextView.ScrollRangeToVisible(new NSRange(10000, 1));
        }

        #region HandleCancelButtonPress
        private void HandleCancelButtonPress(object sender, EventArgs e)
        {
            if (cancel == false)
            {
                cancel = true;
            }
            else
            {
                cancel = false;
                RequestWallpaper();
            }
        }
        #endregion
    }
}
