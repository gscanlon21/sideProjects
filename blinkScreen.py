from PyQt5.QtWidgets import QApplication, QWidget
import sys
import time

# Blinks the screen every 3-4 seconds to alert your eyes to blink
# Only tested on Windows 10
#
# For best results, disable: Animate windows when minimizing and maximizing
# ... from Windows settings


class BlinkScreen(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        # self.palette = QPalette()
        # self.palette.setColor(QPalette.Background,Qt.black)
        # Initially loads white so black doesn't load fast enough
        # self.setPalette(self.palette)

        # 1916*1053, Your screen size, height = height-(heightOfWindowsTaskBar)
        self.setGeometry(0, 0, 1916, 1053)
        # self.setWindowTitle('Blinking') # Not neccesary

        self.show()

    def keyPressEvent(self, e):
        if e.key() == 67:  # ctrl-c
            sys.exit()

    def showEvent(self, event):
        if self.isVisible() is True:
            self.setFocus()

    def focusInEvent(self, event):
        time.sleep(.04)
        self.close()
        try:
            # Between 3 and 4 seconds for average time you should blink
            time.sleep(4)
        except KeyboardInterrupt:
            sys.exit()
        self.initUI()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = BlinkScreen()
    sys.exit(app.exec_())
