﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Linq;


// MacOSx
// Program added to ~/.bash_profile
// mono /Users/{username}/Documents/sideprojects/OrganizeDownloads/OrganizeDownloads/Program.exe


namespace OrganizeDownloads
{
    public class Program
    {
        public static void Main(string[] args)
        {
            RunTeleprompter().Wait();
        }

        private static async Task RunTeleprompter()
        {
            Console.WriteLine("\nThe Downloads folder cleaner is now running...\nPress `q` to quit\n");
            var moveDownloads = MoveDownloads();
            var escapeTask = GetInput();

            await Task.WhenAny(moveDownloads, escapeTask);
        }

        private static async Task MoveDownloads()
        {
            while (true)
            {
                string[] downloadsFiles = Directory.GetFiles("/Users/grahamscanlon/Downloads");
                string[] desktopFiles = Directory.GetFiles("/Users/grahamscanlon/Desktop");
                string[] files = downloadsFiles.Concat(desktopFiles).ToArray();

                foreach (string file in files)
                {
                    string fileExtension = Path.GetExtension(file);
                    switch (fileExtension)
                    {
                        case ".pdf":
                            string filename = Path.GetFileName(file);
                            string newFilePath = "/Users/grahamscanlon/Documents/general/documents";

                            Console.WriteLine($"Moving {filename} to {newFilePath}");
                            File.Move(file, $"{newFilePath}/{filename}");
                            break;
                        case ".png":
                        case ".jpg":
                            filename = Path.GetFileName(file);
                            newFilePath = "/Users/grahamscanlon/Pictures/screenshots";

                            Console.WriteLine($"Moving {filename} to {newFilePath}");
                            File.Move(file, $"{newFilePath}/{filename}");
                            break;
                        case ".dmg":
                        case ".zip":
                        case "": // no extension
                        case ".app":
                            filename = Path.GetFileName(file);
                            newFilePath = "/Users/grahamscanlon/.Trash";

                            Console.WriteLine($"Move {filename} to the Trash?\n(y/n)\n");
                            var keyPressed = Console.ReadKey();
                            if (keyPressed.KeyChar == 121) // y
                            {
                                Console.WriteLine($"\nMoving {filename} to the Trash...");
                                File.Move(file, $"{newFilePath}/{filename}");
                            }
                            else
                            {
                                Console.WriteLine($"\nMoving {filename} to the Trash has been aborted");
                            }
                            break;
                        default:
                            Console.WriteLine($"Unhandled {fileExtension}");
                            break;
                    }
                    // One second
                    await Task.Delay(1000);
                }
                Console.WriteLine("\n\nAll operations completed successfully, will resume in an hour\n\n");
                // Break to only run once
                break;
                // Wait one hour before re-executing
                // await Task.Delay(3600000);
            }
        }

        private static async Task GetInput()
        {
            bool exit = false;
            Action work = () =>
            {
                do
                {
                    var key = Console.ReadKey(true);
                    if (key.KeyChar == 'q')
                    {
                        Console.WriteLine("Now Exiting...");
                        exit = true;
                    }
                } while (!exit);
            };
            await Task.Run(work);
        }
    }
}
